<?php

function _aid_login_register() {

}

function _aid_login_start() {
  if ($provider = aid_get_provider()) {
    $authorizationUrl = $provider->getAuthorizationUrl(
      [
        'redirect_uri' => _aid_get_oauth_redirect_uri($_GET['destination'] ?? '/'),
      ]
    );

    // Get the state generated for you and store it to the session for later verification.
    drupal_session_start();
    $_SESSION[SESSION_AID_STATE] = $provider->getState();

    // Remove _GET[destination] to prevent issues with drupal_goto
    unset($_GET['destination']);
    // Send the user to the authentication page.
    drupal_goto($authorizationUrl, [], 302);
  }

  // Login service not available.
  _aid_graceful_fatal_error('Service not available.');
}

function _aid_login_authorized() {
  global $user;
  $params = drupal_get_query_parameters();
  $auth_code = $params['code'] ?? FALSE;

  // Auth code is required, something is wrong.
  if (!$auth_code) {
    _aid_graceful_fatal_error('Missing oauth code.');
  }

  // Bail out if we get a wrong oauth state.
  if (!_aid_is_valid_state($params)) {
    _aid_graceful_fatal_error('Invalid oauth state.');
  }

  $redirect_uri = _aid_get_oauth_redirect_uri($_GET['destination'] ?? '/');
  $access_token = _aid_get_access_token($auth_code, $redirect_uri);
  if (!$access_token) {
    _aid_graceful_fatal_error('Unable to get an access token.');
  }

  try {
    /** @var \AmediaId\Api\DataModel\Profile $resource_owner */
    $resource_owner = _aid_get_resource_owner($access_token);
    if (!$resource_owner) {
      _aid_graceful_fatal_error('Unable to get a resource owner.');
    }
    _aid_oauth_login_register($resource_owner);
    if (user_is_logged_in()) {
      // Mark that the current session has been opened via aID
      $_SESSION[SESSION_VIA_AID] = REQUEST_TIME;
      // Permanently store the token associated with the user.
      aid_store_access_token($user, $access_token);
    }
    else {
      _aid_graceful_fatal_error('External login failed.');
    }

    // Send the user to the final destination.
    drupal_goto();
  }
  catch (\Exception $e) {
    _aid_graceful_fatal_error('Unexpected exception.');
  }

}


/**
 * Build the "authorized" Return URI, used both in the authorization phase and in
 *  the token retrieval.
 *
 * @param string $destination
 *   Destination path
 *
 * @return string Return URI with an encoded destination query
 */
function _aid_get_oauth_redirect_uri(string $destination = '/') {
  // Build the final redirect uri.
  return url('oauth/authorized/aid', [
    'query'    => ['destination' => $destination],
    'absolute' => TRUE,
  ]);
}

/**
 * Checks if the current OAuth2 state is valid.
 *
 * @param array $url_query
 *
 * @return bool
 */
function _aid_is_valid_state(array $url_query): bool {
  $query_state = $url_query['state'] ?? FALSE;
  $session_state = $_SESSION[SESSION_AID_STATE] ?? FALSE;

  if ($query_state && $session_state) {
    return ($query_state <=> $session_state) === 0;
  }

  return FALSE;
}
