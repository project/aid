<?php

use Ramsalt\OAuth2\Client\Provider\AmediaAid;

/**
 * Administrative form for aMedia aID
 * Path: `/admin/config/people/aid`
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 *   Drupal Admin Form
 */
function aid_admin_form($form, &$form_state) {

  $form['oauth_info'] = [
    '#type'         => 'fieldset',
    '#title'        => t('OAuth Configuration'),
    '#description'  => t("Those information are essential for Oauth configuration"),
    '#tree'         => FALSE,
    '#collappsible' => TRUE,
    '#collapsed'    => TRUE,
  ];

  $form['oauth_info']['aid_oauth_id'] = [
    '#type'          => 'textfield',
    '#title'         => t("Client ID"),
    '#description'   => t("OAuth ClientID provided by aMedia"),
    '#default_value' => variable_get('aid_oauth_id'),
  ];

  $form['oauth_info']['aid_oauth_secret'] = [
    '#type'          => 'textfield',
    '#title'         => t("Client Secret"),
    '#description'   => t("OAuth ClientSecret provided by aMedia"),
    '#default_value' => variable_get('aid_oauth_secret'),
  ];

  $available_scopes = AmediaAid::getAllowedScopes();
  $form['oauth_info']['aid_oauth_enabled_scopes'] = [
    '#type'          => 'checkboxes',
    '#options'       => $available_scopes,
    '#title'         => t('OAuth Scopes'),
    '#description'   => t("This defines which OAuth scopes will be requested on authentication"),
    '#default_value' => variable_get('aid_oauth_enabled_scopes', []),
  ];

  return system_settings_form($form);
}
