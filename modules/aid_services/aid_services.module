<?php

use AmediaId\Api\DataModel\SubscriptionSource;
use AmediaId\Api\OauthUserServices;
use AmediaId\Api\OauthUserServicesInterface;

/*
 * ############################## Drupal Hooks ##############################
 *
 * Functions listed below are implementing Drupal hooks.
 *
 */

/**
 * Implements hook_form_FORM_ID_alter() for .
 */
function aid_services_form_aid_admin_form_alter(&$form, &$form_state, $form_id) {
  $form['services_info'] = [
    '#type'         => 'fieldset',
    '#title'        => t('aID services configuration'),
    '#description'  => t('Extra configuration used with <a href="@url">aID Services</a>', ['@url' => 'https://developer.api.no/aid/OAuth-services']),
    '#tree'         => FALSE,
    '#collappsible' => TRUE,
    '#collapsed'    => TRUE,
  ];

  // Get the configured publication domain or try to derive it from the current site.
  $default_publication_domain = variable_get('aid_publication_domain');
  if (!$default_publication_domain) {
    global $base_url;
    $default_publication_domain = parse_url($base_url, PHP_URL_HOST);
  }
  $form['services_info']['aid_publication_domain'] = [
    '#type'          => 'textfield',
    '#title'         => t("Publication domain"),
    '#description'   => t("The domain of the publication domain as registered with aMedia (eg. www.glomdalen.no)"),
    '#default_value' => $default_publication_domain,
  ];
}


/*
 * ############################## Public Toolbox ##############################
 *
 * Functions listed in this section can be freely called by other modules.
 *
 */


/**
 * Loads the OAuth2 client provider for aID.
 * @return \AmediaId\Api\OauthUserServicesInterface
 */
function aid_services_get_provider(): OauthUserServicesInterface {
  // We do not need to set any configuration as we will use only the Service API capabilities.
  $provider = new OauthUserServices();

  drupal_alter('aid_services_provider', $provider);

  return $provider;
}

/**
 * @param \stdClass $account
 *   Optional drupal user object, fallback to the current logged in user.
 *
 * @return \AmediaId\Api\DataModel\Profile|null
 *   User profile on aID or null on failure.
 */
function aid_services_get_user_profile(stdClass $account = NULL) {
  $token = aid_login_get_access_token($account);
  if (!$token) {
    // No token = unknown access.
    return NULL;
  }

  /** @var \AmediaId\Api\DataModel\Profile $profile */
  $profile = _aid_get_resource_owner($token);
  return $profile;
}

/**
 * Checks if the user has an access feature to a publication.
 *
 * @param \stdClass $account
 *   Optional drupal user object, fallback to the current logged in user.
 * @param string|string[]|\AmediaId\Api\DataModel\AccessFeatureType[] $features
 *   One or multiple features. Accepts a string, an array of strings or an array of AccessFeatureType.
 * @param string $publication_domain
 *   Optional override of the publication domain, by default will use the configuration: aid_services_publication_domain
 *
 * @return bool|null
 *   Access response, or NULL if could not load.
 */
function aid_services_check_user_access(stdClass $account = NULL, $features = 'newspaper', string $publication_domain = NULL) {
  global $user;
  // Fallback to logged in user if not provided.
  if (!isset($account->uid)) {
    $account = $user;
  }

  $token = aid_login_get_access_token($account);
  if (!$token) {
    // No token = unknown access.
    return NULL;
  }

  // Get the default value for Pub. Domain if it's not provided.
  if (empty($publication_domain)) {
    $publication_domain = aid_services_publication_domain();
  }

  // Contact the API and return the result.
  return aid_services_get_provider()
    ->userHasAccess($token, $publication_domain, $features);
}

/**
 * Loads the user's Subscriptions based on an access feature.
 *
 * @param \stdClass $account
 *   Optional drupal user object, fallback to the current logged in user.
 * @param string|null $publication_domain
 *   Optional override of the publication domain, by default will use the configuration: aid_services_publication_domain
 *
 * @return \AmediaId\Api\DataModel\SubscriptionList|null
 *   The loaded subscription list from aID, or NULL if could nto be loaded.
 */
function aid_services_get_user_subscriptions(stdClass $account = NULL, string $publication_domain = NULL) {
  global $user;
  // Fallback to logged in user if not provided.
  if (!isset($account->uid)) {
    $account = $user;
  }

  $token = aid_login_get_access_token($account);
  if (!$token) {
    // No token = unknown access.
    return NULL;
  }

  if (empty($publication_domain)) {
    $publication_domain = aid_services_publication_domain();
  }

  // The only one supported currently by aID.
  $source = SubscriptionSource::createInfosoft();

  // Contact the API and return the result.
  return aid_services_get_provider()
    ->getUserSubscriptionList($token, $publication_domain, $source);
}

/**
 * Gets the publication_domain for the current site.
 *
 * If not configured as variable it will log a notice and try to fetch it based on the current
 * website domain.
 *
 * @return string
 */
function aid_services_publication_domain() {
  $publication_domain = variable_get('aid_publication_domain');
  if (!$publication_domain) {
    watchdog('aid_services', "Missing configuration: Publication domain.", WATCHDOG_NOTICE);
    global $base_url;
    $publication_domain = parse_url($base_url, PHP_URL_HOST);
  }

  return (string) $publication_domain;
}

/*
 * ############################## Private Toolbox ##############################
 *
 * Functions listed below are to be considered internal and not used in other modules.
 *
 */
